﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Jobs;
using UnityEngine;

public class CreateCapsuleSystem : JobComponentSystem
{
    protected override void OnCreate()
    {
        base.OnCreate();

        var instance = EntityManager.CreateEntity(
                        //ComponentType.ReadWrite<Translation>(),
                        //ComponentType.ReadWrite<Rotation>(),
                        ComponentType.ReadOnly<LocalToWorld>(),
                        ComponentType.ReadOnly<RenderMesh>(),
                        ComponentType.ReadOnly<RenderBounds>());
        //EntityManager.SetComponentData(instance, new Translation { Value = new float3(0, 0, 0) });
        //EntityManager.SetComponentData(instance, new Rotation { Value = new quaternion(0, 0, 0, 0) });

        EntityManager.SetComponentData(instance, new LocalToWorld { Value = new float4x4(rotation: quaternion.identity, translation: new float3(0, 0, 0)) });

        var rHolder = Resources.Load<GameObject>("ResourceHolder").GetComponent<ResourceHolder>();
        EntityManager.SetSharedComponentData(instance, new RenderMesh { mesh = rHolder.mesh, material = rHolder.material });
        EntityManager.SetComponentData(instance, new RenderBounds { Value = new AABB() });
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        return inputDeps;
    }
}
