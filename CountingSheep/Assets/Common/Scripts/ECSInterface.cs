﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine.UI;

public class ECSInterface : MonoBehaviour
{
    [SerializeField] private GameObject tankPrefab;
    [SerializeField] private GameObject palmTreePrefab;
    [SerializeField] private Text sheepCountText;
    [SerializeField] private Text tankCountText;

    private World world;
    private EntityManager entityManager;

    // Start is called before the first frame update
    private void Start()
    {
        world = World.DefaultGameObjectInjectionWorld;
        entityManager = world.GetExistingSystem<MoveSystem>().EntityManager;
        Debug.Log("All Entities: " + world.GetExistingSystem<MoveSystem>().EntityManager.GetAllEntities().Length);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            float3 position = new float3(UnityEngine.Random.Range(-10, 10), 2, UnityEngine.Random.Range(-10, 10));
            Instantiate(tankPrefab, position, quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            var settings = GameObjectConversionSettings.FromWorld(world, null);
            var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(palmTreePrefab, settings);
            var instance = entityManager.Instantiate(prefab);
            var position = transform.TransformPoint(new float3(UnityEngine.Random.Range(-10, 10), 2, UnityEngine.Random.Range(-10, 10)));
            entityManager.SetComponentData(instance, new Translation { Value = position });
            entityManager.SetComponentData(instance, new Rotation { Value = quaternion.identity });
        }
    }

    public void ShowSheepCount()
    {
        EntityQuery entityQuery = entityManager.CreateEntityQuery(ComponentType.ReadOnly<SheepData>());
        sheepCountText.text = entityQuery.CalculateEntityCount().ToString();
    }

    public void ShowTankCount()
    {
        EntityQuery entityQuery = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TankData>());
        tankCountText.text = entityQuery.CalculateEntityCount().ToString();
    }
}
