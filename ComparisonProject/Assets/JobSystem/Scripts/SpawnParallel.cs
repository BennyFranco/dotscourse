﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;

namespace Sheep.JobSystem
{
    public struct MoveJob : IJobParallelForTransform
    {
        public void Execute(int index, TransformAccess transform)
        {
            transform.position += 0.1f * (transform.rotation * new Vector3(0, 0, 1));
            if (transform.position.z > 50)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, -50);
            }
        }
    }

    public class SpawnParallel : MonoBehaviour
    {
        [SerializeField]
        private GameObject sheepPrefab;
        private Transform[] allSheepTransforms;

        private const int numSheep = 15000;

        private MoveJob moveJob;
        private JobHandle jobHandle;
        private TransformAccessArray transforms;

        // Start is called before the first frame update
        private void Start()
        {
            allSheepTransforms = new Transform[numSheep];
            for (int i = 0; i < numSheep; i++)
            {
                Vector3 position = new Vector3(Random.Range(-50, 50), 0.23f, Random.Range(-50, 50));
                GameObject sheep = Instantiate(sheepPrefab, position, Quaternion.identity);
                allSheepTransforms[i] = sheep.transform;
            }

            transforms = new TransformAccessArray(allSheepTransforms);
        }

        // Update is called once per frame
        private void Update()
        {
            moveJob = new MoveJob { };
            jobHandle = moveJob.Schedule(transforms);
            JobHandle.ScheduleBatchedJobs();
        }

        private void LateUpdate()
        {
            jobHandle.Complete();
        }

        private void OnDestroy()
        {
            transforms.Dispose();
        }
    }
}