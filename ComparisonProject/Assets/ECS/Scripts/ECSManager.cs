﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Sheep.ECS
{
    public class ECSManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject sheepPrefab;
        private EntityManager manager;

        private const int numSheep = 15000;

        // Start is called before the first frame update
        void Start()
        {
            manager = World.DefaultGameObjectInjectionWorld.EntityManager;

            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(sheepPrefab, settings);

            for (int i = 0; i < numSheep; i++)
            {
                var instance = manager.Instantiate(prefab);
                var position = new float3(UnityEngine.Random.Range(-50, 50), 0.23f, UnityEngine.Random.Range(-50, 50));
                manager.SetComponentData(instance, new Translation { Value = position });
                manager.SetComponentData(instance, new Rotation { Value = quaternion.identity });
            }
        }
    }
}