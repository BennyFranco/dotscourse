﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep.Classic
{
    public class Spawn : MonoBehaviour
    {
        [SerializeField]
        private GameObject sheepPrefab;

        private const int numSheep = 15000;

        // Start is called before the first frame update
        void Start()
        {
            for (int i = 0; i < numSheep; i++)
            {
                Vector3 position = new Vector3(Random.Range(-50, 50), 0.23f, Random.Range(-50, 50));
                Instantiate(sheepPrefab, position, Quaternion.identity);
            }
        }
    }
}