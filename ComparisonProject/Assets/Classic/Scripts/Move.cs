﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep.Classic
{
    public class Move : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            transform.Translate(0, 0, 0.1f);
            if (transform.position.z > 50)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, -50);
            }
        }
    }
}