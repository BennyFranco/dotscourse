﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class LargeCubeSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var jobHandle = Entities
            .WithName("MoveSystem")
            .ForEach((ref Translation position, ref Rotation rotation, ref NonUniformScale scale, ref LargeCubeData data) =>
            {
                position.Value -= 0.01f * math.up();
            })
            .Schedule(inputDeps);

        return jobHandle;
    }
}
