﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;

namespace Planets
{
    public class MoveSystem : JobComponentSystem
    {
        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            float deltaTime = Time.DeltaTime;
            float speed = 10f;
            float3 pivot = new float3(0);

            var jobHandle = Entities
                   .WithName("MoveSystem")
                   .ForEach((ref Translation position, ref Rotation rotation, ref AsteroidData data) =>
                   {
                       rotation.Value = quaternion.LookRotation(pivot - position.Value, math.up());
                       float rotSpeed = deltaTime * speed * 1 / math.distance(position.Value, pivot);
                       position.Value = math.mul(quaternion.AxisAngle(new float3(0, 1, 0), rotSpeed), position.Value - pivot) + pivot;
                   })
                   .Schedule(inputDeps);

            return jobHandle;
        }

    }
}