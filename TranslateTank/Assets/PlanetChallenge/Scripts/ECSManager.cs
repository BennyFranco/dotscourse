﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Planets
{
    public class ECSManager : MonoBehaviour
    {
        [SerializeField] private GameObject asteroidPrefab;

        private EntityManager manager;

        private const int asteroidsQuantity = 500;

        void Start()
        {
            manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
            var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(asteroidPrefab, settings);

            for (int i = 0; i < asteroidsQuantity; i++)
            {
                var instance = manager.Instantiate(prefab);
                float x = math.sin(i) * UnityEngine.Random.Range(25, 50);
                float y = UnityEngine.Random.Range(-3, 2);
                float z = math.cos(i) * UnityEngine.Random.Range(25, 100);
                float3 scale = new float3(UnityEngine.Random.Range(0.001f, 0.005f), UnityEngine.Random.Range(0.001f, 0.005f), UnityEngine.Random.Range(0.001f, 0.005f));
                //float3 scale = new float3(0.001f, UnityEngine.Random.Range(0.000001f, 0.001f), 0.001f);
                var position = transform.TransformPoint(new float3(x, y, z));
                manager.SetComponentData(instance, new Translation { Value = position });
                manager.SetComponentData(instance, new Rotation { Value = quaternion.identity });
                manager.SetComponentData(instance, new NonUniformScale { Value = scale });
            }

        }
    }
}
